package programmes;

import java.util.Scanner;

public class Demo1 {

    public static void main(String[] args) {
        
        // Declaration de 3 variables x, y , z de type entiers ( x, y, z: emplacements en mémoire )
        int x, y, z;
    
        // Declaration d'un objet de type scanner, il sera désigné par la variable clavier  
        // pour obtenir des infos à partir du clavier
        // new Scanner(System.in) permet de créer cet objet )
        Scanner  clavier=new Scanner(System.in);
        
        // affiche le texte ecrit entre guillemets 
        System.out.println("Bonjour tout le Monde!");
        
        // affiche le texte ecrit entre guillemets
        System.out.println("Entrez le premier nombre x");
        
        // Attend la saisie d'un nombre entier au clavier et l'enregistre dans la variable x
        x=clavier.nextInt();
        
        System.out.println("Entrez le deuxième nombre y");
        y=clavier.nextInt();
    
        // calcul  de x+y ( contenu de x + contenu de y )et enregistrement du résultat 
        // dans la variable z
        
        z=x+y;
        
        // affiche le texte entre guillemets :  "La somme ... vaut: " ( remarquez l'espace après le :)
        // suivi( rôle de signe + ) du contenu de la variable z 
        
        System.out.println("La somme de x et y vaut: " + z);
    }
}
