
package exercices;

import java.util.Scanner;


public class Exo10 
{
  public static void main(String[] args)
  {
    Scanner clavier=new Scanner (System.in);
    int taille,age,morpho;
    String sexe;
        
    float poidsIdeal_L, poidsIdeal_C ;
        
    System.out.println("Calcul de votre poids ideal selon les formules de Lorentz et de Creff");
        
    System.out.println("\nQuel est votre taille en cm ? ");
    taille=clavier.nextInt();
        
    System.out.println("\nQuel est votre âge ? ");
    age=clavier.nextInt();
        
    System.out.println("\nQuel est votre sexe ? ");
    sexe=clavier.next();
        
    System.out.println("\nQuel est votre morphologie (1 2 ou 3)");
    System.out.println("\n 1: Fine  2: Normale  3: Large\n");
    morpho=clavier.nextInt();
        
    if (sexe.equals("F"))
    {
      poidsIdeal_L=taille-100-(taille-150)/2.5f;
    }
    else poidsIdeal_L=taille-100-(taille-150)/4f;
    if(morpho==1)
    { 
            poidsIdeal_C=(taille-100+age/10)*0.9f*0.9f;
    }
    else if(morpho==2)
         {
            poidsIdeal_C=(taille-100+age/10)*0.9f;
         }
         else
         { 
            poidsIdeal_C=(taille-100+age/10)*0.9f*1.1f;
         }
        
          System.out.println("Poids idéal selon la formule de Lorentz : " +poidsIdeal_L + " kg");
          System.out.println("Poids idéal selon la formule de Creff : " +poidsIdeal_C + " kg"); 
        }       
    }

    
    
    

