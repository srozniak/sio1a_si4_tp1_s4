
package exercices;

import java.util.Scanner;

public class Exo06 {
    
    public static void main(String[] args) {
         
        // Obtention du clavier
        Scanner clavier= new Scanner(System.in);
        
        // Déclaration des variables
        
        // ENTREES
        int     nbAdultes;
        int     nbEnfants;
        int     nbEtudiants;
        String  jourPromo;
       
        //RESULTATS
        float   montant;
        float   prixMoyen;
          
        //<editor-fold defaultstate="collapsed" desc="A VOUS DE COMPLETER ">
       
        System.out.println("Nombre d'adultes?");
        nbAdultes=clavier.nextInt();
        
        System.out.println("Nombre d'enfants?");
         nbEnfants=clavier.nextInt();
        
        System.out.println("Nombre d'étudiants?");
        nbEtudiants=clavier.nextInt();
        
        //</editor-fold>
        
        System.out.println("Jour de promotion? (Répondre par oui ou non )");
        jourPromo=clavier.next();
       
        //<editor-fold defaultstate="collapsed" desc="A VOUS DE COMPLETER">
        
        montant=7*nbAdultes+4*nbEnfants+5.5f*nbEtudiants;
        
        // On teste si on est un jour de promo
        // Attention le test d'égalité se fait avec .equals pour le type String
        
        if( jourPromo.equals( "oui" ) ) { montant=montant*0.8f;}
        
        prixMoyen=montant/(nbEnfants+nbAdultes+nbEtudiants);
        
        
        //Affichage formaté du contenu de la variable  montant
        // 1) on utilise System.out.printf
        // 2) Dans le texte entre guillemets %3.2f sera remplacé par la valeur de montant
        // 3) %5.2f signifie un nombre réel  de 5 chiffres dont 2 après à virgule 
        // 4) notez bien la virgule entre le texte entre guillemets et la variable montant
        System.out.printf("\nMontant dû: %5.2f €\n", montant);
        
        //Affichage formaté du contenu de la variable prixMoyen
        // Même fonctionnement que ci-dessus
        System.out.printf("\nPrix moyen d'une place: %5.2f €\n\n", prixMoyen);
        
         //</editor-fold>
   }
}
