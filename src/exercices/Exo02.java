
package exercices;

import java.util.Scanner;

public class Exo02 {

     
    public static void main(String[] args) {
        
      Scanner clavier=new Scanner(System.in);     
      
      int age;
      System.out.println("Quel est votre age");
      
      age=clavier.nextInt();
      
      // Tester si age <18
      
      if(age <18){
          
          // on place ici, entre ces accolades, les  instructions exécutées 
          //si le test réussit
          System.out.println("Vous êtes mineur"); 
      }
      else{
          //on place ici, entre ces accolades,  les instructions exécutées 
          //si le test échoue
          System.out.println("Vous êtes majeur"); 
      } 
      
    }

}
