
package exercices;

import java.util.Scanner;

public class Exo08 {

    public static void main(String[] args) {
        
         // Déclaration d'un dispositif de saisie de type Scanner nom de variable-->clavier
         Scanner clavier = new Scanner(System.in);
    
         // Déclaration des variables en entrée
         float salaire;
         int   nbEnf, anc;
         
         // Déclaration des variables de sortie ( résultats)
         float primeSalaire, primeEnf, primeAnc, primeVac;
        
         // Saisie du salaire
         System.out.println("Quel est votre salaire?");
         salaire= clavier.nextFloat();
    
         // Calcul de la prime dépendant du salaire -> primeSalaire
         
         if       ( salaire <= 1600f ) primeSalaire=180f;
         else if  ( salaire <= 2200f ) primeSalaire=150f;
         else if  ( salaire <= 3000f ) primeSalaire=100f;
         else                          primeSalaire= 60f;
   
         // Saisie du nombre d'enfants de moins de 18 ans
         System.out.println("Combien avez d'enfants de moins de 18 ans? ");
         nbEnf=clavier.nextInt();
        
         
         // Calcul de la prime dépendant du nombre d'enfants mineurs-> primeEnf
         primeEnf=nbEnf*110f;
        
         // Saisie de l'ancienneté
         System.out.println("Quelle est votre anciennetée?");
         anc=clavier.nextInt();
        
         // calcul de la prime dépendant de l'ancienneté->  primeAnc
         
         if      ( anc >= 10)  primeAnc= 150f;
         else if ( anc >=  5)  primeAnc= 120f;
         else if ( anc >=  1)  primeAnc=  90f;
         else                  primeAnc=   0f; 
         
         // Calcul de la prime de vacances ( total des 3 primes calculées précédemment)
         
         primeVac= primeSalaire+primeEnf+primeAnc;
       
         // On passe une ligne
         System.out.println();
         
         // Affichage des 3 primes 
         System.out.println("Prime de base: "      + primeSalaire + " €");
         System.out.println("Prime Enfants: "      + primeEnf     + " €");
         System.out.println("Prime d'ancienneté: " + primeAnc     + " €");
        
         // Affichage de la prime de vacances
         System.out.println("\nVotre prime de vacances est de "+ primeVac +"€");
 
         // On passe une ligne pour la présentation
         System.out.println();
    }
    
}
