
package exercices;

import java.util.Random;
import java.util.Scanner;


public class Exo11 
{
  public static void main(String[] args) 
  {
        Scanner  scan   = new Scanner(System.in);
        
        boolean  trouve = false;
        
        int nbADeviner;
        int nbPropose;
        int nbEssais=0;
        
        
        Random rd=new Random();
        nbADeviner=rd.nextInt(1000)+1;
        
        System.out.println("Essayez de le deviner avec le moins d'essais possible.");
        
        while ( ! trouve ){
        
        nbEssais++;
            
        System.out.println("Proposez un nombre entre 1 et 1000");
        nbPropose=scan.nextInt();
              
        if (nbPropose>nbADeviner){
            System.out.println("Trop grand.");
        }
        else if(nbPropose<nbADeviner){
            System.out.println("Trop petit.");
        }
        else {System.out.println("Bravo vous avez trouvé en "+nbEssais + " essais !");
        trouve=true;}
        }
        System.out.println("");
    }    
    
}
