
package exercices;

import java.util.Scanner;

public class Exo01 {
 public static void main(String[] args) {
        
       
        // Déclaration des variables en entrée
        float largeur, longueur;
        
        // Déclaration des variables de sortie 
        // c'est à dire  contenant les résultats
        // les noms de variables doivent commencer par une minuscule
        // Règle Camel
        float perimetre, surface;
        
        // Déclaration et création d'un dispositif de saisie
        // désigné par la variable clavier
        Scanner clavier= new Scanner(System.in);
        
        // Affichage d'une question
        // et acquisation d'un nombre réel
        // via le clavier
        System.out.println("Largeur?");
        largeur=clavier.nextFloat();
        
        System.out.println("Longueur?");
        longueur=clavier.nextFloat();
        
        // calculs et affectations
        perimetre= 2*(longueur+largeur);
        surface= longueur*largeur;
        
        // Affichage
        System.out.println("Le périmètre est: " + perimetre);
        
        System.out.println("La surface   est: " + surface);
        
    }

}
