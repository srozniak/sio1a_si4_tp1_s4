
package exercices;

import java.util.Scanner;

public class Exo03 {
 
    public static void main(String[] args) {
        
        // Déclaration et création d'un dispositif de saisie
        // désigné par la variable clavier
        Scanner clavier= new Scanner(System.in);
        
        // Déclaration des variables en entrée
        float ca;
        
        // Déclaration des variables de sortie 
        // c'est à dire  contenant les résultats
        // les noms de variables doivent commencer par une minuscule
        // Règle Camel
        float com;
        
        // Affichage d'une question
        // et acquisation d'un nombre réel
        // via le clavier
        System.out.println("Chiffre d'affaires réalisé? ");
        ca=clavier.nextFloat();
  
        
        // Tester si ca <10000
        if ( ca < 10000){
           // on place ici, entre ces accolades, les  instructions exécutées 
           //si le test réussit
            com = ca*0.02f;  
        }
        else{
            //on place ici, entre ces accolades,  les instructions exécutées 
          //si le test échoue
            com = 200 +(ca-10000)*0.04f;   
        }
        
        // Affichage du résultat
        System.out.println("La commission est de:  "+com+" €\n");
    }

}
